# README for mandel

Mandel is a tool to compute high resolution images
of the [Mandelbrot set](https://en.wikipedia.org/wiki/Mandelbrot_set).


## Workflow

* Edit the color gradient using [GIMP](https://www.gimp.org/)
* Edit the definition file (`.man` extension)
* On MacOS
  * `cp "/Users/$USER/Library/Application Support/GIMP/2.8/gradients/..." gradients`
  * `mandel .../(definition file name).man`
  * Convert the resulting [PPM file](https://de.wikipedia.org/wiki/Portable_Anymap)
    to PNG (e.g. using ImageMagick convert tool)

You can use tools like [XaoS](https://xaos-project.github.io/) to find interesting regions
of the Mandelbrot set and then use *mandel* to create a high-resolution rendering.

## Example

Please see the files in `artwork/katja`.


# How to edit color gradients

## In GIMP

* Use Preferences -> Folders -> Gradients and add the 'gradients' directory.
* Use the Gradient editor (command-G, or Windows -> Dockable Dialogs ->
  Gradient Editor)
* On MacOS, saved gradients end up in 
  `/Users/$USER/Library/Application Support/GIMP/x.y/gradients` 
  with a `.ggr` extension.


## Definition file parameters

Definitions are stored in files commonly with a `.man` extension.

* `center` ({ real, imaginary }): center of view window 
* `width` (positive real): width of window 
* `aspect_ratio`: width / height of window
* `diagonal` (vector of positive real): { width , height }.
* `angle` (real; radians): window is rotated by angle (counter-clockwise)
* `size_x` (integer): number of pixels on x axis.  The number on the 
   y axis is computed from `width` and `aspect_ratio`.

## Notes

* Either diagonal or the pair `width` and `aspect_ratio` must be provided!

# Compile

Mandel is written in ISO C++.  Any modern C++ compiler should be
able to compile it.

## TODO

* At the moment, this is broken
* An update to `cmake` is needed, see e.g. KTrax.
* Add [cpp-lib](https://gitlab.com/gewesp/cpp-lib) as a submodule.

# ChangeLog

## Tue Oct 23 18:04:36 CEST 2001

* added README and this file
* added `width` and `aspect_ratio` specifications
* support for `.mandelrc` file
* Version 0.3

## Sat Nov 20 13:43:13 CET 2004

* Compiles with cpp-lib 0.9.4 prerelease
* Version 0.4.0.

## Sat Oct  9 18:04:15 UTC 2021

* Improved README.md
* Split into user and internal documentation
* Moved to its own repo

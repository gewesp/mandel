Specifying the region
=====================

Use a combination of center and diagonal or center, width and
aspect_ratio to specify a rectangular axis-parallel area of the complex
plane.

center = { x , y }
  Center of area is at c = x + iy.

diagonal = { z , w }
  Area is bounded by c - d/2 and c + d/2, where d = z + iw.

aspect_ratio = a
width = w
  With d = w + iw/a, area is bounded as above by c - d/2 and c + d/2.
  
angle = alpha
  After the above specification, the area can be rotated in
  counter-clockwise direction by specifying an angle [radians].
  Notice that the image will appear to rotate *clockwise* for increasing
  alpha.

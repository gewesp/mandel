# Internal documentation

## Gimp gradients

### GIMP `apps/gradient_header.h`
```
typedef enum
{
  GRAD_LINEAR = 0,
  GRAD_CURVED,
  GRAD_SINE,
  GRAD_SPHERE_INCREASING,
  GRAD_SPHERE_DECREASING
} grad_type_t;

typedef enum
{
  GRAD_RGB = 0,  /* normal RGB */
  GRAD_HSV_CCW,  /* counterclockwise hue */
  GRAD_HSV_CW    /* clockwise hue */
} grad_color_t;


struct _grad_segment_t
{
  double       left, middle, right; /* Left pos, midpoint, right pos */
  double       r0, g0, b0, a0;          /* Left color */
  double       r1, g1, b1, a1;          /* Right color */
  grad_type_t  type;                    /* Segment's blending function
*/
  grad_color_t color;             /* Segment's coloring type

  struct _grad_segment_t *prev, *next; /* For linked list of segments */
};
```

### GIMP `apps/gradient.c`
```
      if (sscanf (line, "%lf%lf%lf%lf%lf%lf%lf%lf%lf%lf%lf%d%d",
                  &(seg->left), &(seg->middle), &(seg->right),
                  &(seg->r0), &(seg->g0), &(seg->b0), &(seg->a0),
                  &(seg->r1), &(seg->g1), &(seg->b1), &(seg->a1),
                  &type, &color) != 13)
```
Check the function `gradient_get_color_at()`!


# Description of the color mapping and gradient file

The gradient file defines a mapping of a value v in [0,1] to RGBA colors.  This is red, green
and blue intensity and alpha (opacity; always 1 in my case), all in [0,1].

The first line of the gradient file is the text 'GIMP Gradient'.  The second line has the
number of 'segments'.  Each of the remaining lines defines a 'segment' of the mapping, that is,
a subinterval of [0,1] such that [0,1] is the union of all subintervals in the file.

The values on one segment line are:  l m r c_l c_r 1 0 l and r are the left and right endpoint
of the interval, resp. and are mapped to the RGBA colors c_l and c_r.  The 'middle' value m is
mapped to the mean value of c_l and c_r.  The 1 means that a certain spline interpolation is
used in between the values.  The 0 means that we're operating in RGBA space (as opposed to
(hue, saturation, value) triples or other ways of specifying a color).

To sum up, first the speed with which the Mandelbrodt iteration diverges is normalized to the
above v in [0,1] and then mapped to a color by the gradient.  If the iteration does not diverge
within 30000 iterations, (i.e., the absolute value stays below 2), the color is black.

See also section 7.10, Gradients, of the GIMP documentation:
http://docs.gimp.org/en/gimp-concepts-gradients.html

# TODO

Future config files might look like the following:
```
area "full" {

  center = { -.75 , 0 }
  diagonal = { 2.5 , 2.5 }

  # optional
  angle = 0

  # evtl. aspect_ratio=...

}


quality "medium" {

  bailout = 2
  max_iter = 1000
  smooth = 3
  
  # supersampling subdivision
  aai = 3

  # subdivide if color difference between neighbours exceeds threshold
  threshold = .01

  # pixels on the x axis
  resolution = 400

}


color "beautiful" {

  gradient = "gradients/sepp"

  skew = .1

}
```

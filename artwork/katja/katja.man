# definitions for mandel-0.2


center = { -0.75851198 , -0.076429539 }
diagonal = { 0.00023548006 , 0.00023548006 }

angle = 0


size_x = 1500
# size_x = 700

gradient = "katja.grad"

max_iter = 30000
smooth = 3
bailout = 4

normalize = { 300 , .02 }

adaptive_antialias = 6
threshold = 0

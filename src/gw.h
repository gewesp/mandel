//
// $Id: gw.h,v 1.4 2012/09/19 14:09:41 gwesp Exp $
//

#ifndef GW_H
#define GW_H

#define GCC_EXPLICIT_BUG

#include <vector>
#include <list>
#include <map>

#include <iosfwd>
#include <limits>
#include <memory>
#include <stdexcept>
#include <cstddef>
#include <utility>
#include <iterator>
#include <sstream>
#include <numeric>
#include <iomanip>
#include <cmath>

#include "boost/array.hpp"


namespace gw {


// math constants

extern const double pi ;


// forward declarations...

struct int_rng ;
struct ring_elt ;


// some primtive modular arithmetic; works only if values are roughly
// below square root of max integer value...

typedef unsigned long integer ;



void check_moduli( const ring_elt& , const ring_elt& ) ;

struct ring_elt {

  ring_elt( integer _x , integer _modulus ) 
    : x(_x % _modulus) , modulus(_modulus) { }

  const ring_elt& operator++() { if( ++x == modulus ) x = 0 ; return *this ; }

  integer representant() const
    { return x ; }

  integer get_modulus() const
    { return modulus ; }

  // friend std::ostream& operator<<( std::ostream& , const ring_elt& ) ;

  friend ring_elt operator*( const ring_elt& , const ring_elt& ) ;
  friend ring_elt operator*( const ring_elt& , integer         ) ;
  friend ring_elt operator*( integer         , const ring_elt& ) ;

  friend ring_elt operator+( const ring_elt& , const ring_elt& ) ;
  friend ring_elt operator+( const ring_elt& , integer         ) ;
  friend ring_elt operator+( integer         , const ring_elt& ) ;

  friend bool operator==( const ring_elt& , const ring_elt& ) ;

  friend void check_moduli( const ring_elt& , const ring_elt& ) ;

private:
  
  ring_elt() { }

  integer x , modulus ;
  
} ;

struct poly {

  // polynomial with given coefficient list; will reduce modulo _modulus
  poly( const std::vector<integer>& _coeff , integer _modulus ) ;

  // zero polynomial of degree d.  ya know what i mean.
  poly( int d , integer _modulus )
    : coeff( d + 1 ) , modulus( _modulus ) { }
  
  // the monomial a x^d
  poly( int d , ring_elt a )
    : coeff( d + 1 ) , modulus( a.get_modulus() )
  {
    coeff[d] = a.representant() ;
  }

 
  // construct a random polynomial of degree <= d; modulus will be
  // r.get_max()
  // if monic, then monic of degree = d
  
  poly( int d , int_rng& r , bool monic = false ) ;

  
  // application operator

  ring_elt operator() ( const ring_elt& ) const ;

  ring_elt operator() ( integer ) const ;


  bool is_zero() const ;
  
  bool is_monic() const { return coeff.back() == 1 ; }
  

  integer get_modulus() const
    { return modulus ; }
  
  friend std::ostream& operator<<( std::ostream& , const poly& ) ;

  // generate next polynomial of degree <= d, in lexicographic order
  // FIXME!  no bounds checkin yet

  poly& advance( int d ) ;
 
private:

  std::vector<integer> coeff ;

  integer modulus ;

} ;



class rng {

public:

  rng() ;

  // return a double in [0,1)
  
  double get_next() ;


private:
  
  static std::auto_ptr<std::ifstream> random_bytes ;

} ;


struct int_rng {
  
  int_rng( integer , integer ) ;


  // return a PRN in [a,b)

  integer get_next() ;
 
  integer get_max() const { return b; }
  

private:
  
  rng r ;

  integer a , b ;

} ;
  

// some utility routines

inline unsigned long pack( std::vector< unsigned char >::const_iterator i ) {

  unsigned long ret = 0 ;

  for( unsigned j = 0 ; j < sizeof( unsigned long ) ; ++j )
    ret |= static_cast< unsigned long >( *( i++ ) << 8 * j ) ;

  return ret ;

}


inline void 
unpack( unsigned long w , std::vector< unsigned char >::iterator i ) {

  for( unsigned j = 0 ; j < sizeof( unsigned long ) ; ++j ) {

    *( i++ ) = w & 0xff ;
    w >>= 8 ;

  }

}


template< typename T > inline int sign( T x ) {

  if( x < 0 ) return -1 ;
  if( x > 0 ) return 1 ;
  return 0 ;

}


inline double clamp( double x , double left , double right ) {

  if( x < left ) return left ;
  if( x > right ) return right ;
  return x ;

}


inline double round( double x ) {

  return std::floor( x + .5 ) ;

}


// print msg to std::cerr and exit with exit code 1

void die( std::string const& ) ;


// check if a key exists in a map and return associated value

template< typename K , typename V >
V find_value( const K& key , const std::map< K , V >& m )
{

  typename std::map< K , V >::const_iterator i = m.find( key ) ;
  if( i == m.end() )
    throw std::runtime_error( "key " + key + " not found" ) ;

  return i->second ;

}


// note: order of typenames is important here!
template< typename D , typename S > 
inline D checked_cast( const S& source )
{

  D dest = static_cast< D >( source ) ;

  if( static_cast< S >( dest ) != source )
    throw std::runtime_error( "bad gw::checked_cast" ) ;

  return dest ;

}


// FIXME !!!  all of this is a q&d hack.
// Replace by cpp-lib matrix.

template <class T> class matrix {

public:

  typedef std::size_t size_type ;

  typedef T& reference ;
  typedef const T& const_reference ;

  typedef std::vector< T > container_type ;
  

  // empty matrix

  matrix() { m = n = 0 ; }


  // data will be default-initialized (means zero for built-in types)

  matrix( size_type m , size_type n )
    : m( m ) , n( n ) , data ( m * n ) { }

  // matrix( size_type _m )
  //   : m(_m) , n(_m) , data ( _m*_m ) { }

  // C must be a container of T
  // create diagonal matrix with entries from cont
  

#ifndef GCC_EXPLICIT_BUG
  template<class C> explicit matrix ( const C& cont )
    : m( cont.size() ) , n( cont.size() ) , data( m*n )
  {
    typename C::const_iterator p = cont.begin() ;
    size_type i = 1 ;
    do 
      ( *this )( i , i ) = *( p++ ) ;
    while( ++i <= m ) ;
  }
#endif


  reference operator() ( size_type i , size_type j )
  { range_check( i , j ) ; return data[ (i-1)*n + j-1 ] ; }

  const_reference operator() ( size_type i , size_type j ) const
  { range_check( i , j ) ; return data[ (i-1)*n + j-1 ] ; }

  size_type rows() const { return m ; }
  size_type columns() const { return n ; }
  
  matrix< T >& resize( size_type m_ , size_type n_ )
  {

    m = m_ ;
    n = n_ ;
    
    data.resize( m * n ) ;

    return *this ;

  }


  T max() const { return *std::max_element( data.begin() , data.end() ) ; } ;
  T min() const { return *std::min_element( data.begin() , data.end() ) ; } ;


  // friend std::ostream& operator<< ( std::ostream& , const matrix& ) ;

  
  matrix< T > operator-() ;


  container_type& rep() { return data ; }

  container_type const& rep() const { return data ; }


private:

  size_type m , n ;
  container_type data ;

  void range_check( size_type i , size_type j ) const
  { if( !i || !j || i > m || j > n ) throw std::range_error("matrix") ; }

} ;



// self-explanatory...
//
// usage: std::generate( begin , end , ascending_sequence() ) ;

struct ascending_sequence {

  ascending_sequence() : i( 0 ) { }

  int operator()() { return i++ ; }

private: 
  
  long int i ;

} ;



// list_form and mma_list:
// 
// provide list_form class plus mma_out friend in
// order to output arbitrary sequences as mathematica lists.
// 
// 
// C must be a container which supports const_iterators and has
// begin() and end() members


template< class C > struct is_standard_container {

  enum { result = false } ;

} ;


// don't know why we need both const and non-const versions...

// note that valarray is _not_ a standard container

template< class T > struct is_standard_container< std::vector< T > > 
{ enum { result = true } ; } ;

template< class T > struct is_standard_container< const std::vector< T > > 
{ enum { result = true } ; } ;

template< class T > struct is_standard_container< std::list< T > > 
{ enum { result = true } ; } ;

template< class T > struct is_standard_container< const std::list< T > > 
{ enum { result = true } ; } ;

template< class T , std::size_t s > struct 
is_standard_container< boost::array< T , s > >
{ enum { result = true } ; } ;

template< class T , std::size_t s > struct 
is_standard_container< const boost::array< T , s > >
{ enum { result = true } ; } ;

// ... and possibly other containers


template< class T >
inline std::size_t size( T const& a )
{

  return sizeof( a ) / sizeof( a[ 0 ] ) ;

}


template<class C> struct list_form {

  list_form ( C& c ) : c( c ) { }

  C& container() { return c ; }
  C const& container() const { return c ; }

private:

  C& c ;

} ;


template<class C> struct list_form_out {

  list_form_out ( C const& c ) : c( c ) { }

  C const& container() const { return c ; }

private:

  C const& c ;

} ;


template<class D> 
  std::ostream& operator<< (std::ostream& , const list_form_out<D>& ) ;
  
template<class C>
  std::istream& operator>> (std::istream& , gw::list_form<C>);

template< class D >
inline list_form_out< D > mma_out ( D const& d ) 
{ return list_form_out< D >( d ) ; }
  

template< class D >
inline list_form< D > mma_list ( D& d ) 
{ return list_form< D >( d ) ; }
  

// hex_form---write vectors of unsigned numbers as a single hexadecimal
// number


template< class C > struct hex_form_t {

  template< class D > 
    friend std::ostream& operator<< 
    ( std::ostream& , hex_form_t< D > const & ) ;
  
  template< class D >
    friend hex_form_t< D > hex_form( D const& ) ;


private:

  hex_form_t( C const& c_ ) : c( c_ ) { } ;

  C const& c ;

} ;


template< class C > 
  std::ostream& operator<< 
  ( std::ostream& os , hex_form_t< C > const& hf ) 
{

  std::ostringstream s ;

  s << std::hex
    << std::setfill( '0' ) ;

   for( typename C::const_iterator i = hf.c.begin() ; i != hf.c.end() ; ++i )
     s << std::setw( std::numeric_limits< typename C::value_type >::digits / 4 )
       << static_cast< unsigned long >( *i ) ;

  return os << s.str() ;

}


template< class C >
  hex_form_t< C > hex_form( C const& c )
{

  return hex_form_t< C >( c ) ;

}


} // namespace gw




//////////////////////////////////////////////////////////////////////
// implementation starts here
//////////////////////////////////////////////////////////////////////


inline void gw::check_moduli( const ring_elt& a , const ring_elt& b )
{ if( a.modulus != b.modulus ) throw "incompatible elements" ; }

inline gw::ring_elt gw::operator+
  ( const gw::ring_elt& a , const gw::ring_elt& b )
{
  check_moduli(a,b) ;

  gw::ring_elt ret ;
  ret.modulus = a.modulus ;	// or b.modulus...

  if( ( ret.x = a.x + b.x ) >= ret.modulus ) ret.x -= ret.modulus ;

  return ret ;
}

inline gw::ring_elt gw::operator+
  ( const gw::ring_elt& a , integer b )
{
  return gw::ring_elt( a.x + b , a.modulus ) ;
}


inline gw::ring_elt gw::operator+
  ( integer b , const gw::ring_elt& a )
{
  return gw::ring_elt( a.x + b , a.modulus ) ;
}

inline gw::ring_elt gw::operator*
  ( const gw::ring_elt& a , const ring_elt& b )
{
  check_moduli( a , b ) ;
  return gw::ring_elt( a.x * b.x , a.modulus ) ;
}

inline gw::ring_elt gw::operator*
  ( const gw::ring_elt& a , integer b )
{
  return gw::ring_elt( a.x * b , a.modulus ) ;
}

inline gw::ring_elt gw::operator*
  ( integer b , const gw::ring_elt& a )
{
  return gw::ring_elt( a.x * b , a.modulus ) ;
}

  
#if 0
// Not used right now
template <class T> std::ostream& gw::operator<<
( std::ostream& os , const gw::matrix<T>& m )
{
  typename gw::matrix<T>::size_type i , j ;

  os << "{\n" ;

  for( i = 1 ; i <= m.rows() ; ++i )
  {
    os << "{ " ;

    for( j = 1 ; j <= m.columns() ; ++j ) 
      os << m(i,j) << ( j == m.columns() ? " }" : " , " ) ;

    os << ( i == m.rows() ? "\n}" : ",\n" ) ; 
  }

  return os ;
}
#endif

template< class T > gw::matrix< T > gw::matrix< T >::operator-()
{
  gw::matrix< T > res( rows() , columns() ) ;
  for( typename std::vector< T >::size_type i = 0 ; i < m * n ; ++i )
    res.data[i] = -data[i] ;

  return res ;
}



// list_form & mma_list


template< class C , bool recurse > struct output ; // helper class for partial
// specialisation

  
template<class C>
  std::ostream& gw::operator<< 
  ( std::ostream& os , const gw::list_form_out<C>& l )
{

  output< C , is_standard_container< C >::result >::do_it( os , l ) ;

  return os ;

}


template< class C > struct output< C , false > {

  static void do_it( std::ostream& os , gw::list_form_out< C > const& l )
  {
    os << l.container() ;
  }

} ;


// unfortunately, the straightforwared 
//
// if( is_standard_container< C::value_type >::result ) recurse...
//
// doesn't work!


template< class C > struct output< C , true > {

  static void do_it( std::ostream& os , gw::list_form_out< C > const& l )
  {

    os << '{' ;

    typename C::const_iterator i = l.container().begin() ;

    while( i != l.container().end() ) {

      os << gw::mma_out( *i ) ;

      if( ++i != l.container().end() )
	os << ',' ;

    }
    
    os << '}' ;

  }

} ;
  

// read values of the form { 1 , 2 , ... n }
// into a container which supports push_back
//
// cf. section 21.3.5 in Stroustrup, TCPPL

template< class C , bool recurse > struct input {
  
  static void do_it( std::istream& is , gw::list_form< C >& ) ;
  
} ;



template< class C > struct input< C , false > {

  static void do_it( std::istream& is , gw::list_form< C >& l )
  {
    is >> l.container() ;
  }

} ;


template< class C > struct input< C , true > {

static void do_it( std::istream& is , gw::list_form< C >& l )
{

  C ret ;

  char in = 0 ;

  is >> in ;

  if( in != '{' )
    return is.putback( in ).setstate( std::ios_base::failbit ) ;

  while( is >> in ) {

    if( in == '}' ) break ;

    is.putback( in ) ;

    typename C::value_type elem ;

    if( is >> gw::mma_list( elem ) ) ret.push_back( elem ) ;
    else break ;

    if( is >> in ) {

      if( in == '}' ) break ;
      else if( in != ',' ) {

	is.putback( in ).setstate( std::ios::failbit ) ;
	break ;

      }

    }

  }

  if( is ) l.container() = ret ;

}

} ;



// the actual input operator

template< class C >
std::istream& gw::operator>>( std::istream& is , gw::list_form< C > l )
{

  input< C , gw::is_standard_container< C >::result >::do_it( is , l ) ;

  return is ;

}



#endif // GW_H

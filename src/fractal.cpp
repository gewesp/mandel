//
// $Id: fractal.cc,v 1.3 2007/05/30 16:35:14 gwesp Exp $
//

#include <vector>
#include <complex>
#include <ostream>
#include <cmath>
#include <iostream>
#include <iterator>
#include <algorithm>

#include "cpp-lib/registry.h"
#include "cpp-lib/array_ops.h"
#include "cpp-lib/math-util.h"

#include "graphics.h"
#include "gw.h"

#include "fractal.h"


std::vector< gw::color > compute_line( 
  long y , 
  transformation const& t ,
  mandelbrot_function const& f ,
  fractal_coloring const& c
) {

  std::vector< gw::color > ret( t.get_size_x() + 2 ) ;

  for( long x = -1 ; x <= t.get_size_x() ; ++x ) {

    bool in_set ;
    double val ;

    f( t( x , y ) , in_set , val ) ;

    ret[ x + 1 ] = c( in_set , val ) ;

  }

  return ret ;

}


void write_matrix(
  transformation const& t ,
  mandelbrot_function const& f ,
  std::ostream& os
) 
{

  const long bins = 24 ;

  std::vector< double > bin_count( bins , 0 ) ;


  os << t.get_size_x() << ' ' << t.get_size_y() << '\n' ;

  for( long y = 0 ; y != t.get_size_y() ; ++y ) {
    for( long x = 0 ; x != t.get_size_x() ; ++x ) {
      bool in_set ;
      double val ;

      f( t( x , y ) , in_set , val ) ;

      os << ( in_set ? -1 : val ) << '\n' ;

      if( not in_set ) {
	
	long bin = static_cast< long >( val * bins ) ;

	if( bin >= 0 && bin < bins ) ++bin_count[ bin ] ;

      }
    }
  }


  for( std::vector< double >::iterator i = bin_count.begin() ;
       i != bin_count.end() ;
       ++i )
    *i /= ( t.get_size_x() * t.get_size_y() ) ;

  std::cerr << "bin_count:\n" ;

  std::copy( bin_count.begin() , bin_count.end() ,
             std::ostream_iterator< double >( std::cerr , "\n" ) ) ;
}


void mandelbrot_function::operator()( 
  std::complex< double > c , bool& in_set , double& ret ) const 
{

  complex z = c ;
  long iter = 0 ;


  while( ++iter < max_iter && abs_squared( z ) < bailout_squared ) {
    z = z * z + c ;
  }


  if( iter == max_iter ) {
    
    ret = 0 ;
    in_set = true ;

  } else {

    in_set = false ;

    for( int i = 0 ; i < smooth ; ++i ) z = sqr( z ) + c ;

    ret = 
      iter + smooth + 2 
      - std::log( std::log ( std::abs( z ) ) ) / std::log( 2. ) ;


#if 0    

    // normalize to [0,1]

    ret = 2 * std::atan( ret * skew ) / pi ;


    if( ret <= 0 ) 
      ret = 0 ;

    else
      ret = std::log( ret ) ;


    ret /= std::log( max_iter + smooth + 2 ) ;
#endif

  }
}


transformation transformation_from_registry( 
  cpl::util::registry const& reg , 
  double angle_increment 
)
{
  std::vector< double > const& center = 
    reg.check_vector_double( "center" , 2 ) ;
  
  std::vector< double > diagonal ;
  
  if( reg.is_set( "diagonal" ) )
    diagonal = reg.check_vector_double( "diagonal" , 2 ) ;

  else {

    diagonal.resize( 2 ) ;

    diagonal[ 0 ] = reg.get< double >( "width" ) ;
    diagonal[ 1 ] = diagonal[ 0 ] / reg.get< double >( "aspect_ratio" ) ;

  }


  always_assert( 2 == center  .size() ) ;
  always_assert( 2 == diagonal.size() ) ;

  return transformation( 
    std::complex< double >( center  [ 0 ] , center  [ 1 ] ) ,
    std::complex< double >( diagonal[ 0 ] , diagonal[ 1 ] ) , 
    reg.get< double >( "angle" ) + angle_increment ,
    gw::checked_cast< long >( reg.get< double >( "size_x" ) ) 
  ) ;

}

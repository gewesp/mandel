//
// $Id: graphics.h,v 1.4 2007/05/30 16:35:14 gwesp Exp $
//

#ifndef GW_GRAPHICS_H
#define GW_GRAPHICS_H

#include <utility>
#include <vector>
#include <functional>
#include <iterator>
#include <ostream>
#include <fstream>

#include <cmath>

#include "boost/array.hpp"
#include "cpp-lib/array_ops.h"
#include "cpp-lib/registry.h"

#include "gw.h"


namespace gw {


// red, green and blue

enum { ncol = 3 } ;


// indices of values when using hsv model

enum { hue_index = 0 , saturation_index , value_index } ;

typedef boost::array< double , ncol > color ;


// conversion between color models

color rgb_to_hsv( color const& ) ;

color hsv_to_rgb( color const& ) ;


extern color red , 
	     green , 
	     blue ,

	     black , 
	     white ;

// Determine RGB values from key = { R, G, B } in registry
// Use def if key is not defined
color from_registry( cpl::util::registry const& , 
                     std::string const& key ,
                     color const& def = black) ;


typedef gw::matrix< color > pixmap ;


// write pixmap in ppm(5) format

std::ostream& operator<<( std::ostream& , pixmap const& ) ;


struct color_function : std::unary_function< double , color > {

  virtual ~color_function() { }

  virtual gw::color operator()( double ) const = 0 ;

} ;


// read definion from file

std::auto_ptr< color_function > new_color_function( std::string const& ) ;


struct color_fix : public std::pair< double , color > {
  
  color_fix( double x , color c )
    : std::pair< double , color >( x , c ) { } 
  
} ;


// a function object mapping doubles to colors,
// linearly interpolating between given fixes

struct linear_gradient : gw::color_function {
  
  // [ begin , end ) must be a sequence of (double , c) pairs, where
  // c must be a container containing exactly 3 elements

  template< class I > linear_gradient( I begin , I end ) 
    : fixes( begin , end )
  { setup_fixes() ; }
    

  // read fixes in format {x0,{r0,g0,b0},x1,{r1,g1,b1},...}, either
  // from stream or named file

  explicit linear_gradient( std::istream& ) ;

  explicit linear_gradient( std::string const& ) ;
 

  color operator()( double value ) const ;
  
  
private:
  
  typedef std::vector< color_fix > fixvector_t ;

  fixvector_t fixes ;

  void setup_fixes() ;

  void read_fixes( std::istream& ) ;

} ;


struct gimp_gradient : public gw::color_function {

  explicit gimp_gradient( std::istream& is ) { read_fixes( is ) ; }

  explicit gimp_gradient( std::string const& name ) {

    std::ifstream is( name.c_str() ) ;

    read_fixes( is ) ;

  }


  gw::color operator()( double ) const ;

  
private:
  
  void read_fixes( std::istream& ) ;

  enum type_t {
    GRAD_LINEAR = 0,
    GRAD_CURVED,
    GRAD_SINE,
    GRAD_SPHERE_INCREASING,
    GRAD_SPHERE_DECREASING
  } ;

  enum color_t {
    GRAD_RGB = 0,  /* normal RGB */
    GRAD_HSV_CCW,  /* counterclockwise hue */
    GRAD_HSV_CW    /* clockwise hue */
  } ;



  struct segment {
    double       left, middle, right; // Left pos, midpoint, right pos
    gw::color    c0 ;  // left
    gw::color    c1 ;  // right
    type_t  type ; // Segment's blending function
    color_t color ; // Segment's coloring type
  } ;

  typedef std::vector< segment > segment_list_t ;
  segment_list_t segment_list ;

  mutable segment_list_t::const_iterator last_visited ;

  segment_list_t::const_iterator get_segment_at( double pos ) const ;

} ;


template< typename T > gw::pixmap 
colorize( gw::matrix< T > const& A , gw::color_function const& f )
{

  double max = A.max() , min = A.min() ;

  gw::pixmap ret( A.rows() , A.columns() ) ;

  
  for( std::size_t i = 0 ; i < A.rep().size() ; ++i )
    
      ret.rep()[ i ] = f( ( A.rep()[ i ] - min ) / ( max - min ) ) ;


  return ret ;

}




struct ppm_output_iterator 
: public std::iterator< std::output_iterator_tag , void , void , void , void >
{

  ppm_output_iterator( long x , 
                       long y , 
		       long color_resolution , 
		       std::ostream& os ,
		       bool raw = true ) 
  : raw( raw ) , color_resolution( color_resolution ) , os( &os )
  {
    if( raw && color_resolution > 256 )
      throw 
	std::runtime_error( "raw ppm images can only have 256 color levels" ) ;

    os << ( raw ? "P6\n" : "P3\n" )
       << x << ' ' << y << '\n'
       << color_resolution - 1 << '\n' ;
  }


  ppm_output_iterator& operator*() { return *this ; }
  ppm_output_iterator& operator++() { return *this ; }
  ppm_output_iterator& operator++( int ) { return *this ; }

  ppm_output_iterator& operator=( gw::color const& c ) {

    for( gw::color::const_iterator i = c.begin() ; i != c.end() ; ++i ) {

      double out = std::floor( *i * color_resolution ) ;

      if( out < 0 ) {
	if( raw ) os->put( 0 ) ; 
	else *os << '0';
      }

      else if( out >= color_resolution ) {
	if( raw ) os->put( static_cast< char >( color_resolution - 1 ) ) ; 
	else *os << color_resolution - 1 ;
      }

      else {
	if( raw ) os->put( static_cast< char >( out ) ) ; 
	else *os << out ;
      }

      if( !raw ) *os << '\n' ;

    }

    return *this ;

  }

private:

  bool raw ;

  const double color_resolution ;

  std::ostream* os ;

} ;


} // namespace gw

#endif

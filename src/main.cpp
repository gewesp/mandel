//
// $Id: main.cc,v 1.3 2006/02/14 11:12:44 gwesp Exp $
//

#include <iostream>
#include <fstream>
#include <string>
#include <complex>
#include <exception>
#include <sstream>
#include <iomanip>

#include <cstdlib>
#include <cstdio>

#include "cpp-lib/command_line.h"
#include "cpp-lib/registry.h"
#include "cpp-lib/util.h"

#include "graphics.h"
#include "gw.h"

#include "fractal.h"

using namespace cpl::util ;

namespace {


const opm_entry options[] = { 

  opm_entry( "gradient" , opp( true  , 'g' ) ) ,
  opm_entry( "colorize" , opp( false , 'c' ) ) ,
  opm_entry( "matrix"   , opp( false , 'm' ) ) ,
  opm_entry( "single"   , opp( false , 's' ) ) ,
  opm_entry( "version"  , opp( false , 'v' ) ) ,
  opm_entry( "help"     , opp( false , 'h' ) ) ,

} ;


const int color_depth = 256 ;



void usage() {

  die( 
"usage: mandel [options] filename\n" 
"options:\n"
"  -c , --colorize  Read precalculated values.\n"
"  -m , --matrix    Calculate values and write to filename.matrix .\n"
"  -g , --gradient <gradient>\n"
"                   Use gradient file <gradient>, overriding config file.\n"
"  -s , --single    Render only the first image of a sequence.\n"
"  -h , --help      Display this message and exit.\n"
"  -v , --version   Display software version and exit.\n"
  ) ;

}


std::string const mandelrc() {

  char const* home = std::getenv( "HOME" ) ;
  if( !home ) { throw std::runtime_error( "$HOME undefined" ) ; }

  return std::string( home ) + "/.mandelrc" ;

}


std::string const version() 
{ return "0.4.0" ; }


void print_version_and_exit() {

  std::cout << "This is mandel version " << version() << "." << std::endl ;
  std::exit( 0 ) ;

}


void compute_mandelbrot( 
  registry            const& reg                ,
  mandelbrot_function const& f                  ,
  std::string                filename           ,
  long                const  number_in_sequence 
) {

  double angle_increment = ( number_in_sequence < 0 ) ? 0 :
    number_in_sequence 
    * 360. / reg.get< double >( "frames_per_revolution" ) ;

  if( number_in_sequence >= 0 ) {

    char buf[10] ;

    std::sprintf( buf , "%06ld" , number_in_sequence ) ;

    filename += buf ;

  }


  const transformation t = 
    transformation_from_registry( reg , angle_increment ) ;


  std::auto_ptr< gw::color_function > outside =
    gw::new_color_function( reg.get< std::string >( "gradient" ) ) ;

  std::vector< double > const& normalize = 
    reg.check_vector_double( "normalize" ) ;

  const fractal_coloring c( 
      *outside , 
      gw::from_registry(reg, "inside_color"),
      normalize ,
      reg.get_default("flip", false));


  std::ofstream outfile( ( filename + ".ppm" ).c_str() ) ;

  gw::ppm_output_iterator o( t.get_size_x() , 
                             t.get_size_y() , 
			     color_depth ,
			     outfile ) ;


  fractal_engine( 
    t , 
    f ,
    c , 
    gw::checked_cast< int >( reg.get< double >( "adaptive_antialias" ) ) ,
    gw::checked_cast< int >( reg.get< double >( "threshold"          ) ) ,
    o
  ) ;
}


} // anonymous namespace


int main( int , char** argv ) {

  try {

  command_line cl( options , options + size( options ) , argv ) ;


  if( cl.is_set( "help"    ) ) { usage                 () ; }
  if( cl.is_set( "version" ) ) { print_version_and_exit() ; }

  std::string filename ;


  if( !( cl >> filename ) ) { usage() ; }


  registry reg ;

  // in .mandelrc, throw on redefinitions
  reg.read_from( mandelrc() , grammar() , true  ) ;
  reg.read_from( filename   , grammar() , false ) ;


  if( cl.is_set( "gradient" ) ) {

    reg.add_any
    ( "gradient" , cl.get_arg( "gradient" ) , "command line" , false ) ;

  }


  if( cl.is_set( "colorize" ) ) {

    // read from a matrix and output colors
    
    const std::string infile = filename + ".matrix" ;

    std::ifstream matrix( infile.c_str() ) ;

    long pixels_x , pixels_y ;

    if( !( matrix >> pixels_x >> pixels_y ) )
      throw std::runtime_error( "bad fileformat: " + infile ) ; 


    std::ofstream outfile( ( filename + ".ppm" ).c_str() ) ;

    gw::ppm_output_iterator 
      o( pixels_x , pixels_y , color_depth , outfile ) ;

    // TODO: Oops, lots of duplicated code?
    std::auto_ptr< gw::color_function > outside =
      gw::new_color_function( reg.get< std::string >( "gradient" ) ) ;

    std::vector< double > const& normalize = 
      reg.check_vector_double( "normalize" ) ;
  
    const fractal_coloring c( 
        *outside , 
        gw::from_registry(reg, "inside_color"),
        normalize ,
        reg.get_default("flip", false));

    double val ;

    long count = 0 ;
    
    while( matrix >> val ) {

      ++count ;

      if( val == -1 )
      {	*( o++ ) = c( true , 4711 ) ; }  // 4711 = dummy
      else
      {	*( o++ ) = c( false , val ) ; }

    }
    
    if( count != pixels_x * pixels_y )
      throw std::runtime_error( "matrix file: invalid value count" ) ;

    return 0 ;

  }


  const transformation t = transformation_from_registry( reg , 0 ) ;

  
  const mandelbrot_function f( 
    reg.get< double >( "bailout" ) , 
    gw::checked_cast< long >( reg.get< double >( "max_iter" ) ) , 
    gw::checked_cast< int  >( reg.get< double >( "smooth"   ) )
  ) ;


  if( cl.is_set( "matrix" ) ) {

    std::ofstream outfile( ( filename + ".matrix").c_str() ) ;

    write_matrix( t , f , outfile ) ;

    return 0 ;

  }


  if( reg.is_set( "frames_per_revolution" ) 
      || cl.is_set( "single" ) ) {

    long num_frames = 
      gw::checked_cast< long >( reg.get< double >( "frames_per_revolution" ) ) ;

    for( long frame = 0 ; frame < num_frames ; ++frame )
      compute_mandelbrot( reg , f , filename , frame ) ;

  }

  else compute_mandelbrot( reg , f , filename , -1 ) ;


  } catch( std::exception const& e ) 
  { die( e.what() ) ; }

}

//
// $Id: gimp_gradient.cc,v 1.2 2004/11/20 12:38:50 gwesp Exp $
//

#include "cpp-lib/array_ops.h"

#include "graphics.h"

#include <sstream>
#include <string>


  /* GIMP gradient File format is:
   *
   *   GIMP Gradient
   *   [ Name: ]
   *   number_of_segments
   *   left middle right r0 g0 b0 a0 r1 g1 b1 a1 type coloring XXX XXX
   *   left middle right r0 g0 b0 a0 r1 g1 b1 a1 type coloring XXX XXX
   *   ...
   * TODO: Figure out the XXX
   */


const double EPSILON = 1e-10 ;


/* Calculation functions */

double   calc_linear_factor            (double  middle,
						double  pos);
double   calc_curved_factor            (double  middle,
						double  pos);
double   calc_sine_factor              (double  middle,
						double  pos);
double   calc_sphere_increasing_factor (double  middle,
						double  pos);
double   calc_sphere_decreasing_factor (double  middle,
						double  pos);



gw::color gw::gimp_gradient::operator()( double pos ) const
{
  pos = gw::clamp( pos , 0 , 1 ) ;

  segment_list_t::const_iterator seg = get_segment_at( pos ) ;

  double seg_len = seg->right - seg->left ,
         middle ;

  if (seg_len < EPSILON)
    {
      middle = 0.5;
      pos    = 0.5;
    }
  else
    {
      middle = (seg->middle - seg->left) / seg_len;
      pos    = (pos - seg->left) / seg_len;
    }


  double factor ;
  switch (seg->type) {

    case GRAD_LINEAR:
      factor = calc_linear_factor (middle, pos);
      break;

    case GRAD_CURVED:
      factor = calc_curved_factor (middle, pos);
      break;

    case GRAD_SINE:
      factor = calc_sine_factor (middle, pos);
      break;

    case GRAD_SPHERE_INCREASING:
      factor = calc_sphere_increasing_factor (middle, pos);
      break;

    case GRAD_SPHERE_DECREASING:
      factor = calc_sphere_decreasing_factor (middle, pos);
      break;

    default:

      throw std::runtime_error( "gimp_gradient: unknown gradient type" ) ;

  }


  /* Calculate color components */


  if( seg->color == GRAD_RGB )

    return ::operator+( seg->c0 , ::operator*( factor , ( seg->c1 - seg->c0 ) ) ) ;

  else {

    gw::color hsv0 = rgb_to_hsv( seg->c0 ) ;
    gw::color hsv1 = rgb_to_hsv( seg->c1 ) ;


    gw::color ret ;

    // linear interpolation for saturation and value

    for( int i = gw::saturation_index ; i <= gw::value_index ; ++i )

      ret[ i ] = hsv0[ i ] + factor * ( hsv1[ i ] - hsv0[ i ] ) ;


    const int hue = 0 ;

    switch( seg->color ) {


      case GRAD_HSV_CCW:
	if (hsv0[ hue ] < hsv1[ hue ])

	  ret[ hue ] = hsv0[ hue ] + (hsv1[ hue ] - hsv0[ hue ]) * factor;

	else {

	  ret[ hue ] = 
	    hsv0[ hue ] + (1.0 - (hsv0[ hue ] - hsv1[ hue ])) * factor;

	  if (ret[ hue ] > 1.0)
	    ret[ hue ] -= 1.0;
	}

	break;

      case GRAD_HSV_CW:

	if (hsv1[ hue ] < hsv0[ hue ])

	  ret[ hue ] = hsv0[ hue ] - (hsv0[ hue ] - hsv1[ hue ]) * factor;

	else {

	  ret[ hue ] = 
	    hsv0[ hue ] - (1.0 - (hsv1[ hue ] - hsv0[ hue ])) * factor;

	    if (ret[ hue ] < 0.0)
	      ret[ hue ] += 1.0;
	}

	break;

      default:

	throw std::runtime_error( "gimp_gradient: unknown coloring mode" ) ;

    }

    return gw::hsv_to_rgb( ret ) ;
  }
}


void gw::gimp_gradient::read_fixes( std::istream& is ) {

  {
    std::string magic ;
    std::getline( is , magic ) ;
    if( magic != "GIMP Gradient" ) {
      throw std::runtime_error( 
        "gimp_gradient: invalid gradient file format (bad magic)" 
      ) ;
    }
  }

  std::size_t num_segments = 0;

  {
    std::string line;
    std::getline( is , line ) ;

    if (0 == line.find("Name")) {
      // Later versions have 'Name:'; skip
      std::getline( is , line ) ;
    }
    std::istringstream iss(line);
    iss >> num_segments ;
  }

  if( num_segments < 1 ) {
    throw std::runtime_error( "gimp_gradient: invalid number of segments" ) ;
  }


  segment_list.reserve( num_segments ) ;


  for( std::size_t i = 0 ; i < num_segments ; ++i ) {

    segment seg ;

    double alpha ;   // ignored!!!

    int type , color ;

    std::string line;
    std::getline(is, line);
    std::istringstream iss(line);

    iss >> seg.left 
       >> seg.middle 
       >> seg.right

       >> seg.c0[ 0 ] >> seg.c0[ 1 ] >> seg.c0[ 2 ] >> alpha
       >> seg.c1[ 0 ] >> seg.c1[ 1 ] >> seg.c1[ 2 ] >> alpha

       >> type
       >> color ;
    // XXX XXX ignored at end

    if( !iss ) 

      throw std::runtime_error( "gimp_gradient: bad file format" ) ;

    seg.type = static_cast< type_t >( type ) ;
    seg.color = static_cast< color_t >( color ) ;

    segment_list.push_back( seg ) ;
  }

  last_visited = segment_list.end() ;
}


  
gw::gimp_gradient::segment_list_t::const_iterator
gw::gimp_gradient::get_segment_at( double pos ) const
{
  segment_list_t::const_iterator seg ;


  pos = gw::clamp( pos , 0 , 1 ) ;


  if( last_visited != segment_list.end() )

    seg = last_visited;

  else

    seg = segment_list.begin() ;


  while( seg >= segment_list.begin() && seg < segment_list.end() )

    if (pos >= seg->left)

      if (pos <= seg->right) {

	last_visited = seg ;
	return seg;

      }

      else ++seg ;


    else --seg ;


  // Oops: we should have found a segment, but we didn't

  throw std::runtime_error( "gimp_gradient: no matching segment" ) ;
}


/***** Calculation functions *****/

double calc_linear_factor (double middle,
		    double pos)
{
  if (pos <= middle)
    {
      if (middle < EPSILON)
	return 0.0;
      else
	return 0.5 * pos / middle;
    }
  else
    {
      pos -= middle;
      middle = 1.0 - middle;

      if (middle < EPSILON)
	return 1.0;
      else
	return 0.5 + 0.5 * pos / middle;
    }
}

double
calc_curved_factor (double middle,
		    double pos)
{
  if (middle < EPSILON)
    middle = EPSILON;

  return pow(pos, log (0.5) / log (middle));
}

double
calc_sine_factor (double middle,
		  double pos)
{
  pos = calc_linear_factor (middle, pos);

  return (sin ((-gw::pi / 2.0) + gw::pi * pos) + 1.0) / 2.0;
}

double
calc_sphere_increasing_factor (double middle,
			       double pos)
{
  pos = calc_linear_factor (middle, pos) - 1.0;

  return sqrt (1.0 - pos * pos); /* Works for convex increasing and concave decreasing */
}

double
calc_sphere_decreasing_factor (double middle,
			       double pos)
{
  pos = calc_linear_factor (middle, pos);

  return 1.0 - sqrt(1.0 - pos * pos); /* Works for convex decreasing and concave increasing */
}

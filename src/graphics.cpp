// 
// $Id: graphics.cc,v 1.2 2004/11/20 12:38:50 gwesp Exp $
//

#include <fstream>
#include <exception>
#include <algorithm>

#include "cpp-lib/array_ops.h"
#include "cpp-lib/registry.h"

#include "graphics.h"


gw::color gw::red = {{1,0,0}} ; 
gw::color gw::green = {{0,1,0}} ;
gw::color gw::blue = {{0,0,1}} ;

gw::color gw::black = {{0,0,0}} ;
gw::color gw::white = {{1,1,1}} ;



gw::color gw::from_registry(
    cpl::util::registry const& reg,
    std::string const& key,
    gw::color const& def) {

  if (!reg.is_defined(key)) {
    return def;
  }

  gw::color ret;
  auto const v = reg.check_vector_double(key, 3);
  for  (int i = 0; i < 3; ++i) {
    cpl::util::verify_bounds(v[i], "RGB values", 0.0, 1.0);
    ret[i] = v[i];
  }
  return ret;
}


namespace {

gw::color_fix green_gradient_array[] = {
  gw::color_fix( 0 , gw::black ) , 
  gw::color_fix( 1 , gw::green )
} ;

gw::color_fix flame_array[] = {
  gw::color_fix( 0   , gw::black ) , 
  gw::color_fix( .25 , gw::red ) , 
  gw::color_fix( .75 , gw::red + gw::green ) , 
  gw::color_fix( 1   , gw::white ) , 
} ;

} // anon namespace


#if 0
const gw::linear_gradient gw::green_gradient( 
  green_gradient_array ,
  green_gradient_array + gw::size( green_gradient_array ) 
) ;


const gw::linear_gradient gw::flame( 
  flame_array ,
  flame_array + gw::size( flame_array ) 
) ;
#endif

gw::color linear_interpolate( 
  gw::color_fix const& left , 
  gw::color_fix const& right , 
  double x 
) 
{

  return 
      left.second 
    + ( x - left.first ) / ( right.first - left.first )
      * ( right.second - left.second ) ;

}


template< class T1 , class T2 > bool 
pair_comp_1st( std::pair< T1 , T2 > a , std::pair< T1 , T2 > b )
{

  return a.first < b.first ;
  
}



gw::color gw::linear_gradient::operator() ( double x ) const
{

  gw::color dummy = { { 0 , 0 , 0 } } ;
  // lower_bound is described in ISO [lib.lower.bound], p. 557


  fixvector_t::const_iterator i = 
    std::lower_bound( 
      fixes.begin() , 
      fixes.end() , 
      std::make_pair( x , dummy ) ,
      pair_comp_1st< gw::color_fix::first_type , gw::color_fix::second_type > 
    ) ;
  

  if( i == fixes.begin() )  // all fixes >= x

    return i->second ;


  if( i == fixes.end() ) // all fixes < x

    return ( i - 1 )->second ;  // return color value of last fix


  return linear_interpolate( *( i - 1 ) , *i , x ) ;

}


std::auto_ptr< gw::color_function > 
gw::new_color_function( std::string const& name ) 
{

  std::ifstream is( name.c_str() ) ;
  std::string magic ;

  std::getline( is , magic ) ;

  if( magic == "GIMP Gradient" ) 
    
    return 
      std::auto_ptr< gw::color_function >( new gw::gimp_gradient( name ) ) ;

  else

    return
      std::auto_ptr< gw::color_function >( new gw::linear_gradient( name ) ) ;

}


void gw::linear_gradient::setup_fixes() 
{

  if( !fixes.size() ) throw 
    std::runtime_error( "linear_gradient: at least 1 fix is needed" ) ;


  std::sort( fixes.begin() , fixes.end() , 
    pair_comp_1st< gw::color_fix::first_type , gw::color_fix::second_type > ) ;

}


gw::linear_gradient::linear_gradient( std::string const& fn ) {

  std::ifstream is( fn.c_str() ) ;

  if( !is ) 
    throw std::runtime_error( "linear_gradient: cannot open " + fn ) ;

  read_fixes( is ) ;

}


gw::linear_gradient::linear_gradient( std::istream& is ) {

  read_fixes( is ) ;

}


void gw::linear_gradient::read_fixes( std::istream& is )
{

  char c = 0 ;

  is >> c ;

  if( c != '{' ) {
    
    is.putback( c ) ;
    throw std::runtime_error( "linear_gradient: { expected" ) ;

  }


  while( is >> c ) {

    if( c == '}' ) break ;

    is.putback( c ) ;

    double d ;

    is >> d ;

    c = 0 ; is >> c ;

    if( c != ',' ) {
      
      is.putback( c ) ;
      throw std::runtime_error( "linear_gradient: comma expected" ) ;

    }


    static std::vector< double > ccol ;

    if( ( is >> gw::mma_list( ccol ) ) && ccol.size() == gw::ncol ) {

      gw::color col ;

      std::copy( ccol.begin() , ccol.end() , col.begin() ) ;

      fixes.push_back( gw::color_fix( d , col ) ) ;

    } 
    
    else

      throw std::runtime_error( "linear_gradient: {r,g,b} expected" ) ;
  

    c = 0 ; is >> c ;

    if( c == '}' ) break ;

    if( c != ',' ) {
      
      is.putback( c ) ;
      throw std::runtime_error( "linear_gradient: comma expected" ) ;

    }

  }

  setup_fixes() ;

}



std::ostream& gw::operator<<( std::ostream& os , gw::pixmap const& p )
{

  enum { max_rgb_value = 256 } ;

  os << "P3\n"
     << p.columns() << ' ' << p.rows() << '\n'
     << max_rgb_value - 1 << '\n' ;
  
  
  for( pixmap::size_type i = 1 ; i <= p.rows() ; ++i ) {

    for( pixmap::size_type j = 1 ; j <= p.columns() ; ++j )

      for( int k = 0 ; k < gw::ncol ; ++k ) {

	int out = static_cast< int >( max_rgb_value * p( i , j )[ k ] ) ;
	if( out < 0 ) out = 0 ;
	if( out >= max_rgb_value ) out = max_rgb_value - 1 ;

	os << out << ' ' ;

      }


    os << '\n' ;

  }
  
  return os ;
}


gw::color gw::rgb_to_hsv( gw::color const& rgb )
{
  gw::color hsv ;

  double& h = hsv[ gw::hue_index ] ;
  double& s = hsv[ gw::saturation_index ] ;
  double& v = hsv[ gw::value_index ] ;
  
  // h = 0 ; /* Shut up -Wall */

  const double max = *std::max_element( rgb.begin() , rgb.end() ) ;
  const double min = *std::max_element( rgb.begin() , rgb.end() ) ;

  v = max;

  const double delta = max - min ;
  s = max == 0 ? 0 : delta / max ;

  if( s == 0 ) h = 0 ;

  else {

    double const& r = rgb[ 0 ] ;
    double const& g = rgb[ 1 ] ;
    double const& b = rgb[ 2 ] ;

    if( r == max)
      h = (g - b) / delta;
    else if ( g == max)
      h = 2 + (b - r) / delta;
    else if (b == max)
      h = 4 + (r - g) / delta;

    h /= 6 ;

    if (h < 0)
      h += 1;
    else if (h > 1)
      h -= 1;
  }

  return hsv ;
}

gw::color gw::hsv_to_rgb( gw::color const& hsv )
{
  gw::color rgb ;

  if( hsv[ 1 ] == 0 )
    std::fill( rgb.begin() , rgb.end() , hsv[ 2 ] ) ;

  else {

    double h =  hsv[ gw::hue_index ] * 6 ;
    double const& s = hsv[ 1 ] ;
    double const& v = hsv[ 2 ] ;

    if( h == 6 ) h = 0 ;

    const double f = h - std::floor( h ) ,
		 p = v * (1 - s) ,
		 q = v * (1 - s * f) ,
		 t = v * (1 - s * (1 - f)) ;

    switch( static_cast< int >( h ) ) {
      case 0:
	rgb[ 0 ] = v;
	rgb[ 1 ] = t;
	rgb[ 2 ] = p;
	break;

      case 1:
	rgb[ 0 ] = q;
	rgb[ 1 ] = v;
	rgb[ 2 ] = p;
	break;

      case 2:
	rgb[ 0 ] = p;
	rgb[ 1 ] = v;
	rgb[ 2 ] = t;
	break;

      case 3:
	rgb[ 0 ] = p;
	rgb[ 1 ] = q;
	rgb[ 2 ] = v;
	break;

      case 4:
	rgb[ 0 ] = t;
	rgb[ 1 ] = p;
	rgb[ 2 ] = v;
	break;

      case 5:
	rgb[ 0 ] = v;
	rgb[ 1 ] = p;
	rgb[ 2 ] = q;
	break;

      default:
	throw std::runtime_error( "hsv_to_rgb: illegal hue value" ) ;
    }
  }

  return rgb ;
}

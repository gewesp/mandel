//
// $Id: gw.cc,v 1.2 2004/11/20 12:38:50 gwesp Exp $
//

#include <iosfwd>
#include <iostream>
#include <fstream>
#include <vector>
#include <cmath>

#include "gw.h"

const char* dev_urandom = "/dev/urandom" ;


const double gw::pi = 3.141592653589793238462643383279502884 ;


gw::ring_elt gw::poly::operator() ( const gw::ring_elt& x ) const
{

  if( modulus != x.get_modulus() ) throw "incompatible elements" ; 
  return (*this)( x.representant() ) ;

}


gw::ring_elt gw::poly::operator() ( gw::integer x ) const
{

  gw::integer x_power = x ;
  gw::integer ret = coeff[0] ;

  std::vector<integer>::const_iterator i = coeff.begin() ;
  ++i ;

  while( true )
  {
    ret = ( ret + *i * x_power ) % modulus ; 
    if( ++i == coeff.end() ) break ;
    x_power = ( x_power * x ) % modulus ; 
  }
  
  return ring_elt( ret , modulus ) ;  // will the compiler optimize the mod
                                      // in ctor away?

}

bool gw::poly::is_zero() const
{
  std::vector<gw::integer>::const_iterator i( coeff.begin() ) ;
  do
    if( *i ) return false ;
  while( ++i != coeff.end() ) ;
  
  return true ;
}


gw::poly& gw::poly::advance( int d )
{
  std::vector<integer>::iterator i( coeff.begin() ) ;

  do
  {
    if( ++(*i) != modulus ) return *this ;
    *i = 0 ;
  }
  while( ++i <= coeff.begin() + d ) ;
  
  return *this ;
}

std::ostream& gw::operator<<( std::ostream& os , const gw::poly& p )
{
  for( std::size_t i = 0 ; i < p.coeff.size() ; ++i )
    os << " + " << p.coeff[i] << "x^" << i ;
  return os ;
}

gw::poly::poly( const std::vector<gw::integer>& _coeff , gw::integer _modulus )
  : coeff(_coeff) , modulus(_modulus) 
{ 
  for( std::vector<gw::integer>::iterator i( coeff.begin() ) ;
       i < coeff.end() ;
       ++i )
    *i %= modulus ;
}

// create random polynomial
gw::poly::poly( int d , gw::int_rng& r , bool monic )
  : coeff(d + 1) , modulus(r.get_max())
{
  if( monic ) coeff[d] = 1 ;

  for( int i = 0 ; i <= ( monic ? d - 1 : d ) ; ++i )
    coeff[i] = r.get_next() ;
}

std::auto_ptr<std::ifstream> gw::rng::random_bytes(0) ;

gw::rng::rng()
{
  if( !random_bytes.get() ) 
    random_bytes.reset( new std::ifstream( dev_urandom ) ) ;
}


double gw::rng::get_next()
{
  // take 7 bytes from fp and pack them as the fractional
  // part of a double
  
  char c ;
  double d = 0 ;
          
  for( int i = 0 ; i < 7 ; ++i )
  {
    random_bytes->get(c) ;
    d = std::ldexp( d + static_cast<unsigned char>(c) , -8 ) ;
  }

  return d ;

}


gw::int_rng::int_rng( gw::integer _a , gw::integer _b )
  : r() , a(_a) , b(_b) { }

gw::integer gw::int_rng::get_next()
{
  return static_cast<gw::integer>( a + r.get_next() * (b - a) ) ;
}


void gw::die( std::string const& msg )
{

  std::cerr << msg << '\n' ;
  std::exit( 1 ) ;

}


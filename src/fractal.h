// 
// $Id: fractal.h,v 1.3 2006/02/14 11:12:43 gwesp Exp $
//

#ifndef FRACTAL_H
#define FRACTAL_H

#include <complex>
#include <algorithm>
#include <cmath>

#include "gw.h"
#include "graphics.h"


typedef std::complex< double > complex ;


template< typename T > inline T sqr( T const& t ) {

  return t * t ;

}


inline double abs_squared( complex const& z ) {

  return sqr( z.real() ) + sqr( z.imag() ) ;

}


struct transformation {

  transformation( 
    std::complex< double > center ,
    std::complex< double > diagonal ,
    double                 angle ,
    long                   size_x
  )
    : size_x( size_x )
  {

    complex exp_phi = std::exp( complex( 0 , - gw::pi * angle / 180 ) ) ;

    x_axis = complex( diagonal.real() , 0 ) * exp_phi ;
    y_axis = complex( 0 , diagonal.imag() ) * exp_phi ;

    origin = center - diagonal * exp_phi / 2. ;

    size_y = 
      gw::checked_cast< long >( 
	gw::round( size_x * std::abs( y_axis ) / std::abs( x_axis ) ) 
      ) ;

  }


  long get_size_x() const { return size_x ; } 
  long get_size_y() const { return size_y ; } 
  

  std::complex< double > operator()( double x , double y ) const {

    return origin + x * x_axis / double( size_x )
                  + y * y_axis / double( size_y ) ;

  }

  
private:

  typedef std::complex< double > complex ;

  complex origin , x_axis , y_axis ;

  long size_x , size_y ;

} ;



struct fractal_coloring {

  fractal_coloring( gw::color_function const& cf , 
                    gw::color const& inside ,
		    std::vector< double > const& normalize ,
                    bool flip) 
    : outside( cf ) , inside( inside ) , normalize( normalize ) , flip( flip )
  { 
    if( normalize.size() != 2 )
      throw std::runtime_error( "normalize must be of form { a , b }" ) ;
  }


  gw::color operator()( bool in_set , double val ) const {
    if( in_set ) {
      return inside;
    }
    double gradient_arg = 
        std::atan( ( val - normalize[ 0 ] ) * normalize[ 1 ] ) * 2 / gw::pi ;

    if( flip ) {
      gradient_arg = 1 - gradient_arg ;
    }
    
    return outside( gradient_arg ) ;
  }


private:

  const gw::color_function& outside ;

  gw::color inside ;

  std::vector< double > normalize ;

  bool flip ;

} ;



struct mandelbrot_function {

  mandelbrot_function( 
    double bailout ,
    long max_iter ,
    int smooth
//    double skew
  ) 
  : bailout_squared( bailout * bailout ) , 
    max_iter( max_iter ) ,
    smooth( smooth )
//    skew( skew )
  { }


  void operator()( std::complex< double > , bool& , double& ) const ;


private:

  double bailout_squared ;

  long max_iter ;

  int smooth ;

//  double skew ;

} ;


std::vector< gw::color > compute_line( 
  long ,
  transformation const& ,
  mandelbrot_function const& ,
  fractal_coloring const&
) ;


void write_matrix( 
  transformation const& ,
  mandelbrot_function const& ,
  std::ostream&
) ;


template< typename o_it > void fractal_engine(

  transformation const& t ,
  mandelbrot_function const& f ,
  fractal_coloring const& c ,

  int aai ,
  double threshold ,

  o_it o 

) {

  using namespace cpl::math ;

  const int extra_lines = 3 ;
  std::vector< std::vector< gw::color > > line( extra_lines ) ;


  for( long y = -1 ; y <= 0 ; ++y )
    line[ y + 1 ] = compute_line( y , t , f , c ) ;

  // line[ y + 1 ].size() == t.get_size_x() + 2


  double aa_count = 0 ;


  for( long y = 0 ; y < t.get_size_y() ; ++y ) {

    // new (long) line 

    line[ extra_lines - 1 ] = compute_line( y + 1 , t , f , c ) ;


    for( long x = 0 ; x < t.get_size_x() ; ++x ) {

      gw::color current = line[ 1 ][ x + 1 ] ;

      // if aai > 1, test neighbourhood of current

      if( 
	aai > 1 && (
	     norm_1( current - line[ 1 ][ x + 2 ] ) >= threshold
	  || norm_1( current - line[ 0 ][ x + 2 ] ) >= threshold
	  || norm_1( current - line[ 0 ][ x + 1 ] ) >= threshold
	  || norm_1( current - line[ 0 ][ x     ] ) >= threshold
	  || norm_1( current - line[ 1 ][ x     ] ) >= threshold
	  || norm_1( current - line[ 2 ][ x     ] ) >= threshold
	  || norm_1( current - line[ 2 ][ x + 1 ] ) >= threshold
	  || norm_1( current - line[ 2 ][ x + 2 ] ) >= threshold
	)
      ) {

	// do anti-aliasing
	
	++aa_count ;
	

	for( int y_mu = 0 ; y_mu < aai ; ++y_mu )
	for( int x_mu = 0 ; x_mu < aai ; ++x_mu ) {

	  // omit center of square

	  if( x_mu == aai / 2 && y_mu == aai / 2 ) continue ;


	  bool in_set ;
	  double val ;

	  f( 
	    t( 
	      x + ( x_mu - aai / 2 ) / double( aai ) ,
	      y + ( y_mu - aai / 2 ) / double( aai ) 
	    ) ,
	    in_set , 
	    val 
	  ) ;

	  current += c( in_set , val ) ;

	}

	current /= ( double( aai ) * aai ) ;

      }


      *o++ = current ;
      

    } // end x loop


    // move buffer one up
    std::copy( line.begin() + 1 , line.end() , line.begin() ) ;

  } // end y loop


  std::cerr << "anti-aliasing performed " << aa_count 
            << " times ("
	    << 100 * aa_count / t.get_size_x() / t.get_size_y()
	    << " percent of all pixels)\n" ;

}


transformation transformation_from_registry( 
  cpl::util::registry const& ,
  double /* angle_increment */
) ; 


#endif // FRACTAL_H

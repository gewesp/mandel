//
// $Id: gradient.h,v 1.2 2004/11/20 12:38:50 gwesp Exp $
//

#ifndef GIMP_GRADIENT_H
#define GIMP_GRADIENT_H


namespace gw {
  
struct gimp_gradient : public gw::color_function {

  gimp_gradient( std::istream& is ) { read_fixes( is ) ; }

  gimp_gradient( std::string const& name ) {

    std::ifstream is( name ) ;

    read_fixes( is ) ;

  }


  gw::color operator()( double ) const ;

  

private:
  
  void read_fixes( std::istream& ) ;

  enum type_t {
    GRAD_LINEAR = 0,
    GRAD_CURVED,
    GRAD_SINE,
    GRAD_SPHERE_INCREASING,
    GRAD_SPHERE_DECREASING
  } ;

  enum color_t {
    GRAD_RGB = 0,  /* normal RGB */
    GRAD_HSV_CCW,  /* counterclockwise hue */
    GRAD_HSV_CW    /* clockwise hue */
  } ;



  struct segment {
    double       left, middle, right; // Left pos, midpoint, right pos
    gw::color    c0 ;  // left
    gw::color    c1 ;  // right
    grad_type_t  type ; // Segment's blending function
    grad_color_t color ; // Segment's coloring type
  } ;

  typedef std::vector< segment > segment_list_t ;
  segment_list_t segment_list ;

  mutable segment_list_t::const_iterator last_visited ;

  segment_list_t::const_iterator get_segment_at( double pos ) const ;

} ;


} // namespace gw

#endif
